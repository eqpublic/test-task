package ru.testtask.api.store;

import com.google.inject.Inject;
import io.qameta.allure.Story;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.testtask.models.Order;
import ru.testtask.models.OrderStatusEnum;
import ru.testtask.service.PetStoreRestModule;
import ru.testtask.steps.StoreSteps;

@Story("Проверки методов раздела store")
@ExtendWith(GuiceExtension.class)
@IncludeModule(PetStoreRestModule.class)
public class CreateOrderTest {

    private Order request;

    @Inject
    private StoreSteps storeSteps;

    @BeforeEach
    public void precondition() {
        request = new Order()
                .setId(0)
                .setComplete(false)
                .setPetId(1)
                .setQuantity(4)
                .setShipDate("2020-09-07T06:34:12.924+0000")
                .setStatus(OrderStatusEnum.PLACED);
    }

    @Test
    @Tags({@Tag("smoke"), @Tag("store")})
    @DisplayName("Проверка добавления нового заказа в магазин")
    public void сreateOrderTest() {
        Order result = storeSteps.order(request);
        storeSteps.checkOrder(result, request);
    }
}
