package ru.testtask.service;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:${env}.properties"})
public interface PetStoreRestConfig extends Config {

    @Key("petstore_url_services")
    String getUrl();
}
