package ru.testtask.service;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.aeonbits.owner.ConfigFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.testtask.retrofit.DefaultCallAdapterFactory;
import ru.testtask.retrofit.FakeSSLSocketFactory;
import ru.testtask.retrofit.FakeTrustManager;

public class PetStoreRestModule extends AbstractModule {

    @Override
    protected void configure() {
        // do nothing
    }

    @Provides
    protected Retrofit providesIbRestService() {
        PetStoreRestConfig config = ConfigFactory.newInstance().create(PetStoreRestConfig.class);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .sslSocketFactory(new FakeSSLSocketFactory(), new FakeTrustManager())
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .addCallAdapterFactory(new DefaultCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(config.getUrl())
                .client(client)
                .build();
    }

    @Provides
    protected StoreService providesLightService(Retrofit retrofit) {
        return retrofit.create(StoreService.class);
    }

}
