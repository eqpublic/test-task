package ru.testtask.service;

import retrofit2.Response;
import retrofit2.http.*;
import ru.testtask.models.Order;

public interface StoreService {

    @GET("v2/store/order/{orderId}")
    Response<Order> getOrder(@Path("orderId") Long orderId);

    @POST("v2/store/order")
    Response<Order> order(@Body Order request);
}
