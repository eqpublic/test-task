package ru.testtask.models;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Order {
    private long id;
    private long petId;
    private int quantity;
    private String shipDate;
    private OrderStatusEnum status;
    private boolean complete;
}
