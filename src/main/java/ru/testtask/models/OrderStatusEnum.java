package ru.testtask.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum OrderStatusEnum {
    PLACED("placed"),
    APPROVED("approved"),
    DELIVERED("delivered");

    @Getter
    private final String orderStatus;
}
