package ru.testtask.retrofit;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


public class DefaultCallAdapterFactory<T> extends CallAdapter.Factory {

    @Override
    public CallAdapter<T, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (returnType.getTypeName().startsWith(Call.class.getName())) {
            return null;
        }
        if (returnType.getTypeName().startsWith(Response.class.getName())) {
            return new ResponseCallAdapter((ParameterizedType) returnType);
        }
        return new InstanceCallAdapter(returnType);
    }

    private class ResponseCallAdapter implements CallAdapter<T, Response<T>> {

        private final ParameterizedType returnType;

        ResponseCallAdapter(ParameterizedType returnType) {
            this.returnType = returnType;
        }

        @Override
        public Type responseType() {
            return returnType.getActualTypeArguments()[0];
        }

        @Override
        public Response<T> adapt(Call<T> call) {
            try {
                return call.execute();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class InstanceCallAdapter implements CallAdapter<T, Object> {

        private final Type returnType;

        InstanceCallAdapter(Type returnType) {
            this.returnType = returnType;
        }

        @Override
        public Type responseType() {
            return returnType;
        }

        @Override
        public Object adapt(Call<T> call) {
            try {
                return call.execute().body();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}