package ru.testtask.steps;

import com.google.inject.Inject;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import ru.testtask.models.Order;
import ru.testtask.service.StoreService;

public class StoreSteps {

    @Inject
    private StoreService storeService;

    @Step("Добавление нового заказа")
    public Order order(Order request) {
        retrofit2.Response<Order> response = storeService.order(request);
        return response.body();
    }

    @Step("Проверка созданного заказа")
    public void checkOrder(Order result, Order expectedResult) {
        Assertions.assertTrue(result.getId() > 0);
        expectedResult.setId(result.getId());
        Assertions.assertEquals(
                result,
                expectedResult
        );
    }
}
